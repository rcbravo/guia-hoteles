$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 4000
    });
    $("#modalContacto").on('shown.bs.modal', function() {
        console.log("ya esta en pantalla");
    });
    $("#modalContacto").on('show.bs.modal', function() {
        console.log("se esta mostrando");
        $(".btn.btn-secondary.btn-reserva").prop('disabled', true);
        $(".btn.btn-secondary.btn-reserva").toggleClass("btn-light");
    });
    $("#modalContacto").on('hide.bs.modal', function() {
        console.log("se esta ocultando");
    });
    $("#modalContacto").on('hidden.bs.modal', function() {
        console.log("ya esta culto");
        $(".btn.btn-secondary.btn-reserva").prop('disabled', false);
        $(".btn.btn-secondary.btn-reserva").toggleClass("btn-light");
    });
});